package zcashrpc

import "encoding/hex"

// Hex encapsulates behaviour related to zcash hex encoding of certain fields like memo and keys
type Hex string

// MakeHex converts a string into a zcash-compatible hex string
func MakeHex(input string) Hex {
	return Hex(hex.EncodeToString([]byte(input)))
}

// Decode returns the raw byte representation of a hex-string
func (h Hex) Decode() ([]byte, error) {
	return hex.DecodeString(string(h))
}
