package zcashrpc

import (
	"encoding/json"
	"strconv"
)

// Method restricts method definitions to constants
type Method string

// Constant block of Method assignments
const (
	GetBlock Method = "getblock"
	GetTransaction Method = "gettransaction"

	ZGetTotalBalance       = "z_gettotalbalance"
	ZListAddresses         = "z_listaddresses"
	ZListReceivedByAddress = "z_listreceivedbyaddress"
	ZSendMany              = "z_sendmany"
	ZValidateAddress       = "z_validateaddress"
)

// zcashRequest encapsulates the common parameters in an api request to a zcash full node
type zcashRequest struct {
	JSONRPCVersion string        `json:"jsonrpc"`
	ID             string        `json:"id"`
	Method         Method        `json:"method"`
	Params         []interface{} `json:"params"`
}

func setupZcashRequest() zcashRequest {
	return zcashRequest{ID: "zcash_api_go", JSONRPCVersion: "1.0"}
}

// NewGetTransaction constructs a properly formatted gettransaction request
func NewGetTransaction(id string) []byte {
	request := setupZcashRequest()
	request.Method = GetTransaction
	request.Params = append(request.Params, id)
	data, _ := json.Marshal(request)
	return data
}

// NewGetBlock constructs a properly formatted getblock request
func NewGetBlock(is int) []byte {
	request := setupZcashRequest()
	request.Method = GetBlock
	request.Params = append(request.Params, strconv.Itoa(is))
	data, _ := json.Marshal(request)
	return data
}


// NewZValidateAddress constructs a properly formatted z_validateaddress request
func NewZValidateAddress(address string) []byte {
	request := setupZcashRequest()
	request.Method = ZValidateAddress
	request.Params = append(request.Params, address)
	data, _ := json.Marshal(request)
	return data
}

// NewZListAddresses constructs a properly formatted z_listaddresses request
func NewZListAddresses() []byte {
	request := setupZcashRequest()
	request.Method = ZListAddresses
	data, _ := json.Marshal(request)
	return data
}

// NewZGetTotalBalance constructs a properly formatted z_gettotalbalance request
func NewZGetTotalBalance(minConfirmations int) []byte {
	request := setupZcashRequest()
	request.Method = ZGetTotalBalance
	request.Params = append(request.Params, minConfirmations)
	data, _ := json.Marshal(request)
	return data
}

// NewZListReceivedByAddress constructs a properly formatted z_listreceivedbyaddress request
func NewZListReceivedByAddress(fromAddress string) []byte {
	request := setupZcashRequest()
	request.Method = ZListReceivedByAddress
	request.Params = append(request.Params, fromAddress)
	data, _ := json.Marshal(request)
	return data
}

// NewZSendMany constructs a properly formatted z_sendmany request
func NewZSendMany(fromAddress string, amounts []ZcashAmount) []byte {
	request := setupZcashRequest()
	request.Method = ZSendMany
	request.Params = append(request.Params, fromAddress)
	request.Params = append(request.Params, amounts)
	data, _ := json.Marshal(request)
	return data
}

// ZcashAmount an object representing an amount to send in z_sendmany
type ZcashAmount struct {
	Address string  `json:"address"`
	Amount  float64 `json:"amount"`
	Memo    Hex     `json:"memo"`
}

// NewZcashAmount generates a new ZcashAmount object with a properly hex encoded memo field
func NewZcashAmount(toAddress string, memo string, amount float64) ZcashAmount {
	return ZcashAmount{Address: toAddress, Memo: MakeHex(memo), Amount: amount}
}

// Transaction encapsulates the result of GetTransaction
// TODO: currently this only supports time so we can obtain an ordered list of transactions
type Transaction struct {
	Time int `json:"time"`
}

// Block
type Block struct {
	Transactions []string `json:"tx"`
}

// ZcashTransaction defines a transaction received to a zaddress
type ZcashTransaction struct {
	TransactionID string  `json:"txid"`
	Amount        float64 `json:"amount"`
	Memo          Hex     `json:"memo"`
	Change        bool    `json:"change"`
	OutIndex      int     `json:"outindex"`

	// The following parameters are not provided by the zcash node
	Address string `json:"address"`
}

// ZcashResult represents the result of a zcash operation. Result can be any one of a number of defined types.
type ZcashResult struct {
	Result interface{} `json:"result"`
}

// ZValidateAddressResponse encapsulates the result returned from a z_validateaddress request
type ZValidateAddressResponse struct {
	IsValid                    bool   `json:"isvalid"`
	Address                    string `json:"address"`
	Type                       string `json:"type"`
	IsMine                     bool   `json:"ismine"`
	Diversifier                Hex    `json:"diversifier"`
	DiversifiedTransmissionKey Hex    `json:"diversifiedtransmissionkey"`
}

// ZGetTotalBalanceResponse encapsulates the result returned from a z_gettotalbalance request
type ZGetTotalBalanceResponse struct {
	Transparent string `json:"transparent"`
	Private     string `json:"private"`
	Total       string `json:"total"`
}
