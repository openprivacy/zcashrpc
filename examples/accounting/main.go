package main

import (
	"encoding/json"
	"fmt"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/openprivacy/zcashrpc"
	"io/ioutil"
	"sort"
	"strings"
	"time"
)

type zcashConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Onion    string `json:"onion"`
	ZAddress string `json:"zaddress"`
}

type transaction struct {
	time        time.Time
	transaction zcashrpc.ZcashTransaction
}

type timeSlice []transaction

func (p timeSlice) Len() int {
	return len(p)
}

func (p timeSlice) Less(i, j int) bool {
	return p[i].time.Before(p[j].time)
}

func (p timeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func main() {
	log.SetLevel(log.LevelDebug)
	configFile, _ := ioutil.ReadFile("accounting.json")
	config := zcashConfig{}
	_ = json.Unmarshal([]byte(configFile), &config)
	var acn connectivity.ACN
	acn, _ = connectivity.StartTor("./", "")
	acn.WaitTillBootstrapped()
	zc := zcashrpc.NewOnionClient(config.Onion, config.Username, config.Password, acn)
	transactions, err := zc.ListReceivedTransactionsByAddress(config.ZAddress)
	if err != nil {
		log.Errorf("Error fetching zcash transactions: %v", err)
	}

	sortedTransactions := make(timeSlice, 0)

	for _, tr := range transactions {
		t, err := zc.GetTransaction(tr.TransactionID)
		log.Infof("Transaction: %v, Err %v", t, err)
		ttime := time.Unix(int64(t.Time), 0)
		sortedTransactions = append(sortedTransactions, transaction{ttime, tr})
	}
	sort.Sort(sortedTransactions)

	// Output a CSV of all transactions
	for _, transaction := range sortedTransactions {
		memoBytes, _ := transaction.transaction.Memo.Decode()
		memo := strings.ReplaceAll(string(memoBytes), "\"", "\"\"")
		memo = strings.ReplaceAll(memo, string([]byte{0x00}), "")
		fmt.Printf("%s,%s,%f,%v,\"%s\"\n", transaction.time, transaction.transaction.TransactionID, transaction.transaction.Amount, transaction.transaction.Change, memo)
	}

}
