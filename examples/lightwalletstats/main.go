package main

import (
	"encoding/json"
	"fmt"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/openprivacy/zcashrpc"
	"io/ioutil"
	"time"
)

type zcashConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Onion    string `json:"onion"`
	ZAddress string `json:"zaddress"`
}

type transaction struct {
	time        time.Time
	transaction zcashrpc.ZcashTransaction
}

type timeSlice []transaction

func (p timeSlice) Len() int {
	return len(p)
}

func (p timeSlice) Less(i, j int) bool {
	return p[i].time.Before(p[j].time)
}

func (p timeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func main() {
	log.SetLevel(log.LevelInfo)
	configFile, _ := ioutil.ReadFile("local.json")
	config := zcashConfig{}
	_ = json.Unmarshal([]byte(configFile), &config)
	zc := zcashrpc.NewLocalClient(config.Username, config.Password)


	txcount := make(map[int]int)
	for i:=760161-((86400*60)/150);i<760161;i++ {
		block, err := zc.GetBlock(i)
		if err != nil {
			log.Errorf("Error fetching zcash transactions: %v", err)
		}
		_,exists := txcount[len(block.Transactions)]
		if !exists {
			txcount[len(block.Transactions)] = 0
		}
		txcount[len(block.Transactions)]++
		if i % 1000 == 0 {
			log.Infof("Processing Block %v", i)
		}
	}
	for txs:=0;txs<5000;txs++ {
		if txcount[txs] > 0 {
			fmt.Printf("%v,%v\n", txs, txcount[txs])
		}
	}

}
