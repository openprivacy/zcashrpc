# zcashrpc

A small go library for interfacing with a zcash full node. Currently supports:

* `gettransaction`
* `z_validateaddress`
* `z_sendmany`
* `z_listreceivedbyaddress`

