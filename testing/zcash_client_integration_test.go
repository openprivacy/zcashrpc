package zcash2cwtch

import (
	"encoding/json"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/openprivacy/zcashrpc"
	"io/ioutil"
	"testing"
)

type ZcashConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func TestGetTotalBalance(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	configFile, _ := ioutil.ReadFile("config.json")
	config := ZcashConfig{}
	_ = json.Unmarshal(configFile, &config)

	zc := zcashrpc.NewLocalClient(config.Username, config.Password)
	result, err := zc.GetTotalBalance(1)
	t.Logf("Result: %v %v", result, err)
	if err != nil {
		t.Errorf("Failed to get total balance: %v %v", result, err)
	}
}

func TestListTransactions(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	configFile, _ := ioutil.ReadFile("config.json")
	config := ZcashConfig{}
	_ = json.Unmarshal(configFile, &config)

	zc := zcashrpc.NewLocalClient(config.Username, config.Password)
	result, err := zc.ListReceivedTransactionsByAddress("zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f")
	t.Logf("Result: %v %v", result, err)
	if len(result) == 0 {
		t.Errorf("Failed to list transactions")
	} else {
		for _, transaction := range result {
			t.Logf("Transaction: %v", transaction)
		}
	}
}

func TestListAddresses(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	configFile, _ := ioutil.ReadFile("config.json")
	config := ZcashConfig{}
	_ = json.Unmarshal(configFile, &config)

	zc := zcashrpc.NewLocalClient(config.Username, config.Password)
	result, err := zc.ListAddresses()
	t.Logf("Result: %v %v", result, err)
	if len(result) == 0 {
		t.Errorf("Failed to list zaddresses")
	}
}

func TestNewZValidateAddress(t *testing.T) {
	log.SetLevel(log.LevelDebug)
	configFile, _ := ioutil.ReadFile("config.json")
	config := ZcashConfig{}
	_ = json.Unmarshal(configFile, &config)

	zc := zcashrpc.NewLocalClient(config.Username, config.Password)
	result, err := zc.ValidateAddress("zs1pjv7eneq9jshw0eyywpruv2cetl74sh84ymdnyv4c4vg8vl5k2qmlv0n7ye77g49lhqkg75v52f")
	t.Logf("Result: %v %v", result, err)
	if err != nil || result.IsValid == false {
		t.Errorf("Failed to validate real zaddress")
	}
}
